const mongoose = require("mongoose");
var schema = mongoose.Schema;
var todoSchema = new schema({

    done: { type: String, default: false},
    name: { type: String,default: false }
});
module.exports = mongoose.model("list", todoSchema);