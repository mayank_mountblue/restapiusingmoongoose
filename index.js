'use strict';
const express = require("express");
var bodyParser = require('body-parser');
const app = express();
const mongoose = require("mongoose");
const list = require("./list");
mongoose.connect("mongodb://localhost:27017/todolist");
//app.use(express.json());
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));
console.log("connected");
app.get("/todolist", (req, res) => {
    list.find({}, function(err, result) {
        if (err) throw err;
        else {
            console.log("result" + JSON.stringify(result));
            res.send(result);
        }
    })
});
app.get("/todolist/:id", (req, res) => {
    list.findOne({ _id: req.params.id }, )
        .exec((err, result) => {
            if (err) throw err;
            else {
                console.log("result" + JSON.stringify(result));
                res.send(result);
            }
        })
});
app.post("/todolist", (req, res) => {

    var data = {
        name: req.body.name,
        done: req.body.done
    };
    console.log("name"+req.body.name);
    var todo = new list(data);
    todo.save((err, result) => {
        if (err) throw err;
        else {
            console.log("result");
            res.send(result);
        }
    });
});
app.put("/todolist/:id", (req, res) => {
    list.update({ _id: req.params.id }, {$set:{ name: req.body.name, done: req.body.done }}, (err, data) => {
        if (err) throw err;
        else {
            res.send(data);
        }
    });
});
app.delete("/todolist/:id", (req, res) => {
    list.remove({ _id: req.params.id }, (err, data) => {
        if (err) throw err;
        else {
            res.send(data);
        }
    });
});
app.listen(3000);


 
