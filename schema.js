const mongoose = require("mongoose");
var schema = mongoose.Schema;
var todoSchema = new schema({
    "_id":{type:Number,require:true},
    "text": { type: String, require: true },
    "done": { type: Boolean, default: false }
});
module.exports = mongoose.model("list", todoSchema);